﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public const decimal initialParkingBalance = 0;
        public const int parkingCapacity = 10;
        public const int paymentTimeInterval = 5;
        public const int logTimeInterval = 60;
        public const int passengerCarFee = 2;
        public const int truckFee = 5;
        public const decimal busFee = 3.5M;
        public const int motorcycleFee = 1;
        public const decimal penaltyCoefficient = 2.5M;
    }
}
