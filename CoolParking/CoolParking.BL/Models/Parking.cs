﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking instance;
        private static readonly object locker = new object();

        public List<Vehicle> vehicles = new List<Vehicle>();
        public decimal balance = 0;
        Parking()
        {

        }

        public static Parking Instance
        {
            get
            {
                lock (locker)
                {
                    return instance ?? (instance = new Parking());
                }
            }
        }

        public void ResetParking()
        {
            instance = null;
        }
    }
}
