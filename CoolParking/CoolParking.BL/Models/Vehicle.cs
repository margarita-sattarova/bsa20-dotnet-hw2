﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private static readonly Random random = new Random();

        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (!Regex.IsMatch(id, @"^[A-Z]{2}\-\d{4}\-[A-Z]{2}$") || balance < 0)
                throw new ArgumentException();
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            char[] characters = new char[4];
            int numberpart;
            for (var i = 0; i < characters.Length; i++)
            {
                characters[i] = (char)(65 + random.Next(0, 26));
            }
            numberpart = random.Next(0, 10000);
            return $"{characters[0]}{characters[1]}-{numberpart:0000}-{characters[2]}{characters[3]}";
        }
    }
}