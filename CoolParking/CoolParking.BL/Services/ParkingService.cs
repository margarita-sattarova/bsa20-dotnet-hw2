﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        List<TransactionInfo> transactions;
        ILogService Log;
        ITimerService WithdrawTimer, Logtimer;
        public decimal currentParkingIncome = 0;
        private Parking parkingInstance;
        public ParkingService(ITimerService withdrawTimer, ITimerService logtimer, ILogService logService)
        {
            transactions = new List<TransactionInfo>();
            Log = logService;
            WithdrawTimer = withdrawTimer;
            Logtimer = logtimer;

            WithdrawTimer.Elapsed += WithdrawTimer_Elapsed;
            Logtimer.Elapsed += Logtimer_Elapsed;

            WithdrawTimer.Start();
            Logtimer.Start();

            parkingInstance = Parking.Instance;
        }

        private void Logtimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            StringBuilder transactionsToLog = new StringBuilder();
            lock (this)
            {
                foreach (var trans in GetLastParkingTransactions())
                {
                    transactionsToLog.AppendLine(trans.ToString());
                    
                }
                transactions.Clear();
                currentParkingIncome = 0;
            }
            Log.Write(transactionsToLog.ToString());


        }

        private void WithdrawTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            lock (this)
            {
                foreach (var vehicle in Parking.Instance.vehicles)
                {
                    decimal sumToPay = CalculateFee(vehicle);
                    vehicle.Balance -= sumToPay;
                    currentParkingIncome += sumToPay;
                    {
                        transactions.Add(new TransactionInfo
                        {
                            TimeOfTransaction = e?.SignalTime ?? DateTime.Now,
                            Sum = sumToPay,
                            vehicleOnParking = vehicle,
                            vehicleBalanceAfterTransaction = vehicle.Balance
                        });
                    }
                }
            }
        }

        decimal CalculateFee(Vehicle vehicle)
        {
            decimal fee = 0;
            decimal balance = vehicle.Balance;
            switch (vehicle.VehicleType)
            {
                case VehicleType.PassengerCar:
                    fee = Settings.passengerCarFee;
                    break;
                case VehicleType.Truck:
                    fee = Settings.truckFee;
                    break;
                case VehicleType.Bus:
                    fee = Settings.busFee;
                    break;
                case VehicleType.Motorcycle:
                    fee = Settings.motorcycleFee;
                    break;
            }

            if (balance > fee)
            {
                TopUpParkingBalance(fee);
                return fee;
            }
            else if (balance <= 0)
            {
                TopUpParkingBalance(Settings.penaltyCoefficient * fee);
                return Settings.penaltyCoefficient * fee;
            }

            TopUpParkingBalance(balance + Settings.penaltyCoefficient * (fee - balance));
            return balance + Settings.penaltyCoefficient * (fee - balance);
        }

        void TopUpParkingBalance(decimal sum) => Parking.Instance.balance += sum;

        public void AddVehicle(Vehicle vehicle)
        {
            if (Parking.Instance.vehicles.Exists(v => v.Id == vehicle.Id))
                throw new ArgumentException("Пользователь ввел неверный Id!");
            if (GetFreePlaces() <= 0)
                throw new InvalidOperationException("К сожалению, на паркинге нет свободных мест");
            Parking.Instance.vehicles.Add(vehicle);
            Console.WriteLine($"Ваше транспортное средство поставлено на паркинг (ID = {vehicle.Id})");
        }

        public void Dispose()
        {
            WithdrawTimer.Dispose();
            Logtimer.Dispose();
            parkingInstance.ResetParking();
        }

        public decimal GetBalance()
        {
            return Parking.Instance.balance;
        }

        public int GetCapacity()
        {
            return Settings.parkingCapacity;
        }

        public int GetFreePlaces()
        {
            return (Settings.parkingCapacity - Parking.Instance.vehicles.Count);
        }

        public TransactionInfo[] GetLastParkingTransactions() => transactions.ToArray();

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(Parking.Instance.vehicles);
        }

        public string ReadFromLog()
        {
            return Log.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            var indexOfVehicle = Parking.Instance.vehicles.FindIndex(v => v.Id == vehicleId);

            if (indexOfVehicle == -1)
                throw new ArgumentException();
            if (Parking.Instance.vehicles[indexOfVehicle].Balance < 0)
                throw new InvalidOperationException();

            Parking.Instance.vehicles.RemoveAt(indexOfVehicle);
            Console.WriteLine($"Your vehicle is removed from the parking");
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
                var index = Parking.Instance.vehicles.FindIndex(v => v.Id == vehicleId);
                if (index == -1)
                    throw new ArgumentException("Ошибка! Транспортного средства с таким Id нет на паркинге.");
                if (sum <= 0)
                    throw new ArgumentException("Ошибка! Вы не можете пополнить баланс транспортного средства на отрицательное число.");
                Parking.Instance.vehicles[index].Balance += sum;
        }
    }
}
