﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using CoolParking.BL.Interfaces;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; set; } = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
        FileStream fileStream;

        public LogService(string _logfilePath)
        {
            LogPath = _logfilePath;
            if (File.Exists(LogPath))
                fileStream = File.Create(LogPath);
            fileStream?.Dispose();
        }

        public LogService()
        {
            if (File.Exists(LogPath))
                fileStream = File.Create(LogPath);
            fileStream?.Dispose();
        }
        public string Read()
        {
            if (!File.Exists(LogPath))
                throw new InvalidOperationException("Ошибка! Заданный файл для считывания не найден.");
            return File.ReadAllText(LogPath);
        }

        public void Write(string logInfo)
        {
            using (StreamWriter streamWriter = new StreamWriter(LogPath, append: true))
            {
                streamWriter.WriteLine(logInfo);
            }
        }
    }
}
