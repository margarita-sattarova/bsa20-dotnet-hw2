﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public double Interval { get; set; }
        private Timer timer = new Timer();

        public event ElapsedEventHandler Elapsed;

        public TimerService()
        {

        }
        public void Start()
        {
            timer.Interval = Interval;
            timer.Elapsed += Elapsed;
            timer.Start();
        }

        public void Stop()
        {
            timer.Stop();
        }

        public void Dispose()
        {
            timer.Dispose();
        }

    }
}