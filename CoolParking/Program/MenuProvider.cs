﻿using System;
using System.Collections.Generic;
using System.Text;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace Program
{
    class MenuProvider
    {
        internal static bool toContinue = true;
        private static int actionChoiceInput;
        private static int vehicleTypeInput;
        private static decimal vehicleBalanceInput;
        private static VehicleType addedVehicleType;
        private static ParkingService parkingService;
        private static Action[] MenuActions;
        readonly static string MenuOptions;

        static MenuProvider()
        {
            MenuOptions = "Выберите действие:\n" +
                  "0 - Выйти \n"
                + "1 - Вывести на экран текущий баланс Парковки \n"
                + "2 - Вывести на экран сумму денег заработанных за текущий период \n"
                + "3 - Вывести на экран количество свободных/занятых мест на парковке \n"
                + "4 - Вывести на экран все Транзакции Парковки за текущий период \n"
                + "5 - Вывести историю транзакций(считав данные из файла Transactions.log)\n"
                + "6 - Вывести на экран список Тр.средств находящихся на Паркинге \n"
                + "7 - Поставить Тр.средство на Паркинг \n"
                + "8 - Забрать транспортное средство с Паркинга \n"
                + "9 - Пополнить баланс конкретного Тр. средства \n";
            MenuActions = new Action[] { Exit, GetParkingBalanceMenu, CurrentParkingEncomeMenu, GetFreePlacesMenu,
                GetCurrentTransactionsMenu, GetTransactionsHistoryMenu, GetVehiclesListMenu, AddVehicleMenu, RemoveVehicleMenu, TopUpVehicleMenu };
        }
        public static void ProvideMenu(ParkingService incomingParkingService)
        {
            MenuProvider.parkingService = incomingParkingService;
            Console.WriteLine(MenuOptions);

            try
            {
                var actionChoiceInputString = Console.ReadLine();
                actionChoiceInput = Convert.ToInt32(actionChoiceInputString);
                MenuActions[actionChoiceInput]();
            }
            catch (FormatException)
            {
                Console.WriteLine("Пользователь ввел не число!");
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("Ошибка ввода. Введите число от 0 до 9.");
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Exception: {exception.Message}");
            }

            Console.WriteLine();
        }

        private static void Exit()
        {
            toContinue = false;
        }

        private static void GetParkingBalanceMenu()
        {
            Console.WriteLine("\nТекущий баланс Парковки: " + parkingService.GetBalance());
        }

        private static void CurrentParkingEncomeMenu()
        {
            Console.WriteLine("\nСумма денег заработанных за текущий период: " + parkingService.currentParkingIncome);
        }

        private static void GetFreePlacesMenu()
        {
            Console.WriteLine($"\nKоличество свободных мест на парковке: {parkingService.GetFreePlaces()} (из {parkingService.GetCapacity()})");
        }

        private static void GetCurrentTransactionsMenu()
        {
            Console.WriteLine("\nТранзакции Парковки за текущий период: ");
            foreach (var item in parkingService.GetLastParkingTransactions())
                Console.WriteLine(item.ToString());
        }

        private static void GetTransactionsHistoryMenu()
        {
            Console.WriteLine("\nИстория транзакций:");
            try
            {
                Console.WriteLine(parkingService.ReadFromLog());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void GetVehiclesListMenu()
        {
            Console.WriteLine("\nCписок Тр. средств находящихся на Паркинге:");
            foreach (var item in parkingService.GetVehicles())
                Console.WriteLine($"Vehicle Id : {item.Id} | Type: {item.VehicleType} | Balance: {item.Balance}");
        }

        private static void AddVehicleMenu()
        {
            Console.WriteLine("\nВыберите тип транспортного средства: ");
            Console.WriteLine("0 - Пассажирский автомобиль");
            Console.WriteLine("1 - Грузовик");
            Console.WriteLine("2 - Автобус");
            Console.WriteLine("3 - Мотоцикл");

            try
            {
                var vehicleTypeInputString = Console.ReadLine();
                vehicleTypeInput = Convert.ToInt32(vehicleTypeInputString);
                addedVehicleType = (VehicleType)(vehicleTypeInput);

                Console.WriteLine("\nВведите баланс транспортного средства: ");

                var vehicleBalanceInputString = Console.ReadLine();
                vehicleBalanceInput = Convert.ToDecimal(vehicleBalanceInputString);
                var addedVehicle = new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), addedVehicleType, vehicleBalanceInput);

                parkingService.AddVehicle(addedVehicle);
            }
            catch (FormatException)
            {
                Console.WriteLine("Пользователь ввел не число!");
            }
            catch (ArgumentException exception)
            {
                Console.WriteLine($"Exception: {exception.Message}"); ;
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Exception: {exception.Message}");
            }
        }

        private static void RemoveVehicleMenu()
        {
            Console.WriteLine("\nВведите Id тр. средства, которое желаете забрать:");
            string vehicleRemoveIdInput = Console.ReadLine();
            try
            {
                parkingService.RemoveVehicle(vehicleRemoveIdInput);
            }
            catch (ArgumentException)
            {
                Console.WriteLine("пользователь ввел неверный Id!");
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("Невозможно забрать транспортное средство, баланс отрицательный!");
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Exception: {exception.Message}");
            }
        }

        private static void TopUpVehicleMenu()
        {
            Console.WriteLine("\nВведите Id тр. средства, баланс которого желаете пополнить:");
            string vehicleTopUpIdInput = Console.ReadLine();
            Console.WriteLine("\nВведите сумму для пополнения:");
            try
            {
                decimal topUpSumInput = Convert.ToDecimal(Console.ReadLine());
                parkingService.TopUpVehicle(vehicleTopUpIdInput, topUpSumInput);
                Console.WriteLine($"Баланс транспортного средства успешно пополнен. Текущий баланс: {Parking.Instance.vehicles.Find(v => v.Id == vehicleTopUpIdInput).Balance}");
            }
            catch (ArgumentException argEx)
            {
                Console.WriteLine(argEx.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
