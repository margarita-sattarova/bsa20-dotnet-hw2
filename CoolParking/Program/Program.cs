﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;

namespace Program
{
    class Program
    {
        static void Main(string[] args)
        {
            var parkingservice = new ParkingService(new TimerService { Interval = Settings.paymentTimeInterval * 1000 }, new TimerService { Interval = Settings.logTimeInterval * 1000 }, new LogService());

            while (MenuProvider.toContinue)
                MenuProvider.ProvideMenu(parkingservice);
        }
    }
}
